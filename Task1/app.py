# app.py

from flask import Flask
from flask import request
from botocore.vendored import requests
import json
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/count_time", methods=['POST'])
def count():
    json_data = request.get_json(silent=True)
    sites = json_data["sites"]
    result = []

    for req in sites:
        if type(req) == str:
            responce = requests.get(req)
            if responce:
                result.append((req, str(responce.elapsed)))
            else:
                result.append((req, responce.status_code))
        else:
            result.append((req, "Not a string."))

    return json.dumps(result)